module spikekafka

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	github.com/google/uuid v1.1.1
	github.com/jawher/mow.cli v1.1.0
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
