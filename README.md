# Spike - Go+Kafka

Testing integration of [Go](https://golang.org/) with [Apache Kafka](https://kafka.apache.org/).
I love me some Kafka. When I have the option to use this as my queue it's *usually* what I compare
all of the other solutions to for the project (it has a good 75/25 win ratio). This is especially
important for my projects that are working in places like [AWS](https://aws.amazon.com/) that
support Kafka through things like [MSK](https://aws.amazon.com/msk/).
 
## Getting Set Up

To get started, you really just need to run [Docker Compose](https://docs.docker.com/compose/):

    docker-compose up -d

That should be all you need, then you can go through the normal Go processes.

## Execution

I recommend doing this in two different terminal windows/tabs. You need to run this first,
obviously:

    go build

### Consumer

This will wait forever until you give it a `Ctrl + C`:

    ./spikekafka consume
    
### Producer

    ./spikekafka produce

## Contributing

Wanna write more things?  I have no real guidlines besides these:

 * Document and comment the hell out of everything
 * You need to make [pull/merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) or I'll ignore you
 * Be patient if I don't see your PR, I get really busy and won't be able to babysit the projects too much (especially these spike repositories)
 * Don't be a douche bag
