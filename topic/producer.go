package topic

import (
	"fmt"
	"github.com/google/uuid"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
	"spikekafka/errors"
)

// Sends a bunch of messages to the queue.
func SendMessages(topic string, messageCount int) {
	// connect Kafka and create the producer
	producer, err := kafka.NewProducer(getConfiguration())
	errors.FailOnError(err,"Unable to create producer")
	defer producer.Close()

	// create a unique ID for the producer
	producerUuid, err := uuid.NewUUID()
	errors.FailOnError(err, "Unable to generate unique Publisher ID")
	producerId := producerUuid.String()

	// delivery report handler for produced messages
	go deliveryReportHandler(producer)

	// produce the messages to the topic
	for index := 0; index < messageCount; index++ {
		fmt.Printf("Sending message: %d\n", index+1)

		err = producer.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Value:          []byte(fmt.Sprintf("Producer[%s], Message[%d]", producerId, index + 1)),
		}, nil)

		errors.FailOnError(err, fmt.Sprintf("Error sending message: %d", index + 1))
	}

	// flush
	producer.Flush(10_000)
}

// Delivery report handler for events on the given producer.
func deliveryReportHandler(producer *kafka.Producer) {
	for event := range producer.Events() {
		switch eventType := event.(type) {
		case *kafka.Message:
			if eventType.TopicPartition.Error != nil {
				fmt.Println(fmt.Errorf("delivery failed: %v", eventType.TopicPartition))
			} else {
				fmt.Printf("Delivery Success to %v", eventType.TopicPartition)
			}
		}
	}
}
