package topic

import (
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
	"log"
	"spikekafka/errors"
)

// Consumes and prints any messages for the given topic.
func ConsumeMessage(topic string) {
	// connect Kafka and create the consumer
	consumer, err := kafka.NewConsumer(getConfiguration())
	errors.FailOnError(err, "Unable to create consumer")

	// subscribe to the topics we want (well... just "topic" for this test)
	err = consumer.SubscribeTopics([]string{topic}, nil)
	errors.FailOnError(err, "Unable to subscribe to topic")

	// consume messages
	forever := make(chan bool)

	go func() {
		for {
			message, err := consumer.ReadMessage(-1)
			errors.FailOnError(err, "Unable to consume messages!")
			log.Printf("Received Message: %s", string(message.Value))
		}
	}()

	<-forever
}
