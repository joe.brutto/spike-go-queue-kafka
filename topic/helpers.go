package topic

import (
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
	"spikekafka/errors"
)

// Retrieves the configuration for connecting to Kafka.
func getConfiguration() *kafka.ConfigMap {
	return &kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"group.id": "spike",
		"auto.offset.reset": "earliest",
	}
}

// "Quietly" closes a consumer
func CloseConsumer(consumer kafka.Consumer) {
	err := consumer.Close()
	errors.LogError(err, "Error closing consumer")
}
