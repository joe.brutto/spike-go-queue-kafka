package main

import (
	"github.com/jawher/mow.cli"
	"os"
	"spikekafka/errors"
	"spikekafka/topic"
)

const QueueTopic = "spike-topic"
const MessageCount = 2048

func main() {
	// depending on what we're doing, launch the appropriate handling
	app := cli.App("Spike: Go+Kafka", "Simple application for testing Go integration with Apache Kafka")

	app.Command("consume", "Consume any messages that get posted to the queue", func(cmd *cli.Cmd) {
		topic.ConsumeMessage(QueueTopic)
	})

	app.Command("produce", "Produce messages on the queue", func(cmd *cli.Cmd) {
		topic.SendMessages(QueueTopic, MessageCount)
	})

	// run the application
	err := app.Run(os.Args)
	errors.FailOnError(err, "Unable to launch the application")
}
